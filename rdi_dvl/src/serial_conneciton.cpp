#include "serial_conneciton.h"
NS_HEAD

uint16 computeChecksum(const uint8_t *data, size_t len) {
  uint16 checksum = 0;
  for (size_t i = 0 ; i<len; i++) {
    checksum = (data[i]+checksum)%65536;
  }
  return checksum;
}

SerialConneciton::SerialConneciton()
{

  port_.reset(new boost::asio::serial_port(io_));
  nh_.reset(new ros::NodeHandle("~"));


  getParams();

  port_->open(param_.port);
  port_->set_option(boost::asio::serial_port_base::baud_rate(param_.baudrate));

  pd5_pub_ = nh_->advertise<rdi_dvl_msgs::PD5Stamped>(param_.pd5_topic,1);
  twist_pub_ = nh_->advertise<geometry_msgs::TwistWithCovarianceStamped>(param_.twist_topic,1);

  pd5_packet = reinterpret_cast< pd5_struct * >(&arr);

  header_.frame_id=param_.frame_id;

}

SerialConneciton::~SerialConneciton(){
  port_->close();
}

void SerialConneciton::getParams(){
  nh_->param<std::string>("port",param_.port, "/dev/ttyUSB0");
  nh_->param<int>("baudrate",param_.baudrate, 38400); //38400
  nh_->param<std::string>("pd5_topic"   ,param_.pd5_topic,    "pd5");
  nh_->param<std::string>("twist_topic" ,param_.twist_topic,  "twist");
  nh_->param<std::string>("frame_id" ,param_.frame_id,  "dvl");
  nh_->param("covariance/velocity", param_.covariance.velocity,0.0001);
}

void SerialConneciton::spinOnce(){
  boost::asio::read(*port_, boost::asio::buffer(&arr,1));
  header_.stamp = ros::Time::now();
//  ROS_INFO("dvl_data_id: %i", arr[0]);
  if(arr[0] == 125){
    boost::asio::read(*port_, boost::asio::buffer(&arr[1],1));
    if(pd5_packet->data_structure==1){
      boost::asio::read(*port_, boost::asio::buffer(&arr[2], sizeof ( pd5_struct)-2 ));

      rdi_dvl_msgs::PD5Stamped msg;
      uint32_t serial_size = ros::serialization::serializationLength(msg.pd5);

      ros::serialization::IStream stream(arr, serial_size);
      ros::serialization::deserialize(stream, msg.pd5);

      if(msg.pd5.checksum != computeChecksum(arr,msg.pd5.n_bytes))
        ROS_WARN_THROTTLE(1.0,"failed checksum");
      else{
        msg.header = header_;
        pd5_pub_.publish(msg);
        //only publish twist if velocites are good
        if(msg.pd5.x_vel_btm!=VEL_INVALID && msg.pd5.y_vel_btm!=VEL_INVALID && msg.pd5.z_vel_btm!=VEL_INVALID){
          geometry_msgs::TwistWithCovarianceStamped twist_msg;
          twist_msg.header = header_;
          twist_msg.twist.twist.linear.x = msg.pd5.x_vel_btm * 0.001;
          twist_msg.twist.twist.linear.y = msg.pd5.y_vel_btm * 0.001;
          twist_msg.twist.twist.linear.z = msg.pd5.z_vel_btm * 0.001;
          auto C = param_.covariance.velocity;
          twist_msg.twist.covariance =
            { C,0,0,0,0,0,
              0,C,0,0,0,0,
              0,0,C,0,0,0,
              0,0,0,0,0,0,
              0,0,0,0,0,0,
              0,0,0,0,0,0};
          twist_pub_.publish(twist_msg);
        }
      }

    }
  }
  else{
    ROS_WARN_THROTTLE(1.0,"Invalid DVL type detected (this is normal on startup)");
  }

}

void SerialConneciton::spin(){
   while(ros::ok()){
     spinOnce();
   }
}

NS_FOOT
