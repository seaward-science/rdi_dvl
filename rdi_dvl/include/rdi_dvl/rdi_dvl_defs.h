#ifndef RDI_DVL_DEFS_H
#define RDI_DVL_DEFS_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

///
///  Namespace stuff
///
#define NS_HEAD namespace rdi_dvl {

#define NS_FOOT }


#endif // RDI_DVL_DEFS_H
