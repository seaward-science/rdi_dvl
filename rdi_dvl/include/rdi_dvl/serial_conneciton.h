#ifndef SERIAL_CONNECITON_H
#define SERIAL_CONNECITON_H

#include "rdi_dvl_defs.h"
#include <ros/ros.h>
#include <geometry_msgs/TwistWithCovarianceStamped.h>
#include <rdi_dvl_msgs/PD5Stamped.h>
#include <ros/serialization.h>
#include <boost/asio.hpp>
#include <boost/asio/serial_port.hpp>

NS_HEAD

#define VEL_INVALID -32768

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef int16_t int16;
typedef int32_t int32;


struct pd5_struct{
  uint8 dvl_data_id;
  uint8 data_structure;
  uint16 n_bytes;
  uint8 system_config;

  int16 x_vel_btm;
  int16 y_vel_btm;
  int16 z_vel_btm;
  int16 e_vel_btm;

  uint16 rng_to_btm[4];

  uint8 bottom_status;

  int16 x_vel_ref_layer;
  int16 y_vel_ref_layer;
  int16 z_vel_ref_layer;
  int16 e_vel_ref_layer;

  uint16 ref_layer_start;
  uint16 ref_layer_end;
  uint8 ref_layer_status;
  uint8 tofp_hour;
  uint8 tofp_minute;
  uint8 tofp_second;
  uint8 tofp_hundredths;
  uint16 bit_result;
  uint16 speed_of_sound;

  int16 temperature;
  uint8 salinity;
  uint16 depth;

  int16 pitch;
  int16 roll;
  uint16 heading;

  int32 dmg_btm_east;
  int32 dmg_btm_north;
  int32 dmg_btm_up;
  int32 dmg_btm_error;

  int32 dmg_ref_east;
  int32 dmg_ref_north;
  int32 dmg_ref_up;
  int32 dmg_ref_error;

  uint16 checksum;
}__attribute__((packed));



class SerialConneciton
{
public:
  SerialConneciton();
  ~SerialConneciton();
  void getParams();
  void spinOnce();
  void spin();

protected:
  struct{
    std::string port;
    int baudrate;
    std::string pd5_topic;
    std::string twist_topic;
    std::string frame_id;
    struct{
      double velocity;
    } covariance;
  }param_;
  ros::NodeHandlePtr nh_;
  uint8_t arr[sizeof ( pd5_struct)];
  boost::asio::io_service io_;
  std::shared_ptr<boost::asio::serial_port> port_;
  pd5_struct * pd5_packet;
  ros::Publisher pd5_pub_;
  ros::Publisher twist_pub_;
  std_msgs::Header header_;
};

NS_FOOT

#endif // SERIAL_CONNECITON_H
